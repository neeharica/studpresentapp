json.extract! presentation, :id, :name, :descrip, :dt, :locat, :created_at, :updated_at
json.url presentation_url(presentation, format: :json)
