json.extract! phistory, :id, :hname, :hphone, :hcost, :hbookdt, :hlocat, :created_at, :updated_at
json.url phistory_url(phistory, format: :json)
