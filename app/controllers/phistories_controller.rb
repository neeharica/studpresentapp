class PhistoriesController < ApplicationController
  before_action :set_phistory, only: [:show, :edit, :update, :destroy]

  # GET /phistories
  # GET /phistories.json
  def index
    @phistories = Phistory.all
  end

  # GET /phistories/1
  # GET /phistories/1.json
  def show
  end

  # GET /phistories/new
  def new
    @phistory = Phistory.new
    @amount = (params[:cost]).to_i * 100
    @ptid = Presentation.find_by id: params[:pt]
    
    
    @phistory.hname = current_user.name
    @phistory.hphone = current_user.phone
    @phistory.hcost = @amount / 100
    @phistory.hbookdt = Time.now
    @phistory.hlocat = @ptid.name
    @phistory.user_id = current_user.id
    @ptid.attending = @ptid.attending + 1
    
    if (!@phistory.save)   
      redirect_to phistories_path
    end
    if (!@ptid.save)
      redirect_to presentations_path
    end
    
  end

  # GET /phistories/1/edit
  def edit
  end

  # POST /phistories
  # POST /phistories.json
  def create
    # Amount in cents
      @amount = (params[:cost]).to_i 
      @ptid = Presentation.find_by id: params[:pt]
      
    
      customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :source  => params[:stripeToken]
      )
    
      charge = Stripe::Charge.create(
        :customer    => customer.id,
        :amount      => @amount,
        :description => 'Rails Stripe customer',
        :currency    => 'usd'
      )
    
    
    rescue Stripe::CardError => e
      flash[:error] = e.message
      @p = Phistory.last
      @p.destroy
      @ptid.attending = @ptid.attending - 1
      if !@ptid.save
        redirect_to presentations_path
      end
      redirect_to presentations_path
    
  end

  # PATCH/PUT /phistories/1
  # PATCH/PUT /phistories/1.json
  def update
   # respond_to do |format|
     # if @phistory.update(phistory_params)
     #   format.html { redirect_to @phistory, notice: 'Phistory was successfully updated.' }
   #     format.json { render :show, status: :ok, location: @phistory }
   #   else
    #    format.html { render :edit }
    #    format.json { render json: @phistory.errors, status: :unprocessable_entity }
    #  end
   # end
  end

  # DELETE /phistories/1
  # DELETE /phistories/1.json
  def destroy
    @phistory.destroy
    respond_to do |format|
      format.html { redirect_to phistories_url, notice: 'Phistory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phistory
      @phistory = Phistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phistory_params
      params.require(:phistory).permit(:hname, :hphone, :hcost, :hbookdt, :hlocat)
    end
end
