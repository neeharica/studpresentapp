require 'notifyuser'
require 'notifyuserobserver'
class PresentationsController < ApplicationController
  before_action :set_presentation, only: [:show, :edit, :update, :destroy]
  before_action :inbef, only: [:index]
  
  # GET /presentations
  # GET /presentations.json
  def index
    @presentations = Presentation.all
    if params[:topic] == "all"
      @presentations = Presentation.all
    elsif params[:topic] != nil
      @presentations = Presentation.where(:id => params[:topic])
    end
    @obj = Notifyuser.new
    @obj.subscribe(Notifyuserobserver.new)
    if user_signed_in?
         current_user.analyse = params[:intd]
         current_user.save
    end
  end

  # GET /presentations/1
  # GET /presentations/1.json
  def show
  end

  # GET /presentations/new
  def new
    @presentation = Presentation.new
    @courses = Course.all.map{ |c| [c.ctype, c.id] }
    @authors = Author.all.map{ |c| [c.aname, c.id] }
  end

  # GET /presentations/1/edit
  def edit
     @authors = Author.all.map{ |c| [c.aname, c.id] }
     @courses = Course.all.map{ |c| [c.ctype, c.id] }
  end

  # POST /presentations
  # POST /presentations.json
  def create
    @authors = Author.all.map{ |c| [c.aname, c.id] }
    @courses = Course.all.map{ |c| [c.ctype, c.id] }
    if current_user.course_id !=0
        @presentation = Presentation.new(presentation_params)
        @author = Author.find_by id: params[:author_id]
        @course = Course.find_by id: params[:course_id]
        if @author == nil
          @presentation.author_id = 0
        else
          @presentation.author_id = @author.id
        end
        @presentation.course_id = @course.id
        respond_to do |format|
          if @presentation.save
            format.html { redirect_to @presentation, notice: 'Presentation was successfully created.' }
            format.json { render :show, status: :created, location: @presentation }
          else
            format.html { render :new }
            format.json { render json: @presentation.errors, status: :unprocessable_entity }
          end
        end
    else
      @course = Course.find_by id: params[:course_id]
      current_user.course_id = @course.id
      
      if current_user.save
        format.html { redirect_to @presentation, notice: 'Presentation was successfully created.' }
        format.json { render :show, status: :created, location: @presentation }
      else
        format.html { render :new }
        format.json { render json: @presentation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /presentations/1
  # PATCH/PUT /presentations/1.json
  def update
    @courses = Course.all.map{ |c| [c.ctype, c.id] }
    @authors = Author.all.map{ |c| [c.aname, c.id] }
    
      
      if current_user.course_id !=0
          @author = Author.find_by id: params[:author_id]
          @course = Course.find_by id: params[:course_id]
          if @author != nil
            @presentation.author_id = @author.id
          end
          if @course != nil
            @presentation.course_id = @course.id
          end
          respond_to do |format|
            if @presentation.update(presentation_params)
              format.html { redirect_to @presentation, notice: 'Presentation was successfully updated.' }
              format.json { render :show, status: :ok, location: @presentation }
            else
              format.html { render :edit }
              format.json { render json: @presentation.errors, status: :unprocessable_entity }
            end
          end
      else
        @course = Course.find_by id: params[:course_id]
        current_user.course_id = @course.id
        if current_user.save
          format.html { redirect_to @presentation, notice: 'Presentation was successfully created.' }
          format.json { render :show, status: :created, location: @presentation }
        else
          format.html { render :new }
          format.json { render json: @presentation.errors, status: :unprocessable_entity }
        end
      end
  end

  # DELETE /presentations/1
  # DELETE /presentations/1.json
  def destroy
    @presentation.destroy
    respond_to do |format|
      format.html { redirect_to presentations_url, notice: 'Presentation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_presentation
      @presentation = Presentation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def presentation_params
      params.require(:presentation).permit(:name, :descrip, :dt, :locat, :course_id, :author_id)
    end
    
    def inbef
        @presentations = Presentation.all
        @phistories = Phistory.all
        @allow = true
        @ptid = 0
        
        @presentations.each do |presentation|
          @phistories.each do |phistory|
            if phistory.hlocat == presentation.name
              if user_signed_in?
                if phistory.user_id == current_user.id
                  @allow = false
                  @ptid = presentation.id
                end
              end
            end
          end
        end
    end
end
