class EventcalsController < ApplicationController
  def index
    @date = params[:date] ? Date.parse(params[:date]) : Date.today
    @allow = false
    @pid = Presentation.find_by id: params[:pid]
    if @pid != nil
      @pdt = @pid.dt
      @pname = @pid.name
    else
      @pdt = Time.now
      @pname = "no event"
    end
  end
end
