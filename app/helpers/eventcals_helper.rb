require 'eventcal'
module EventcalsHelper
  def eventcal(date = Date.today, &block)
    Eventcal.new(self, date, block).table
  end
end
