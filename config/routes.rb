Rails.application.routes.draw do

  get 'umsg/show'

  resources :eventcals
  resources :charges
  resources :phistories
  resources :authors
  resources :presentations
  #get 'eventcals/show'
  devise_for :users
  root 'presentations#index'
  #devise_scope :user do
   # root to: "devise/registrations#edit"
  #end
end
