class AddAuthorIdToPresentations < ActiveRecord::Migration[5.1]
  def change
    add_column :presentations, :author_id, :integer, :null => false, :default => 0
  end
end
