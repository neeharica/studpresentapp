class AddCourseIdToPresentations < ActiveRecord::Migration[5.1]
  def change
    add_column :presentations, :course_id, :integer, :null => false, :default => 0
  end
end
