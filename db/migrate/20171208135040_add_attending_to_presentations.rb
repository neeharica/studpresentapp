class AddAttendingToPresentations < ActiveRecord::Migration[5.1]
  def change
    add_column :presentations, :attending, :integer, :null => false, :default => 0
  end
end
