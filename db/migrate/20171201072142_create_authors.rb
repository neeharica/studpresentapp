class CreateAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :authors do |t|
      t.string :aname, :null => false, :default => "Name"
      t.text :courses, :null => false, :default => "Experience in the courses"
      t.integer :workexp, :null => false, :default => 0
      t.text :descript, :null => false, :default => "Specify your experience"

      t.timestamps
    end
  end
end
