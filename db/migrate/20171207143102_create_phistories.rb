class CreatePhistories < ActiveRecord::Migration[5.1]
  def change
    create_table :phistories do |t|
      t.string :hname, :null => false, :default => "Name"
      t.integer :hphone, :null => false, :default => 0
      t.integer :hcost, :null => false, :default => 0
      t.datetime :hbookdt, :null => true
      t.string :hlocat, :null => false, :default => "Location"

      t.timestamps
    end
  end
end
