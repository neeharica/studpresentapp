class CreateUmsgs < ActiveRecord::Migration[5.1]
  def change
    create_table :umsgs do |t|
      t.text :udescp, :null => true, :default => ""

      t.timestamps
    end
  end
end
