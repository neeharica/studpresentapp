class AddPcostIdToAuthors < ActiveRecord::Migration[5.1]
  def change
    add_column :authors, :pcost_id, :integer, :null => false, :default => 0
  end
end
