class CreatePresentations < ActiveRecord::Migration[5.1]
  def change
    create_table :presentations do |t|
      t.string :name, :null => true, :default => "not specified"
      t.text :descrip, :null => true, :default => "not specified"
      t.datetime :dt, :null => true
      t.string :locat, :null => true, :default => "not specified"

      t.timestamps
    end
  end
end
