class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :ctype, :null => true, :default => "course"

      t.timestamps
    end
  end
end
