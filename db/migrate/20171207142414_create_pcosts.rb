class CreatePcosts < ActiveRecord::Migration[5.1]
  def change
    create_table :pcosts do |t|
      t.integer :cost, :null => true, :default => 5

      t.timestamps
    end
  end
end
