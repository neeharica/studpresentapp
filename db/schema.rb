# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171210150036) do

  create_table "authors", force: :cascade do |t|
    t.string "aname", default: "Name", null: false
    t.text "courses", default: "Experience in the courses", null: false
    t.integer "workexp", default: 0, null: false
    t.text "descript", default: "Specify your experience", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pcost_id", default: 0, null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string "ctype", default: "course"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pcosts", force: :cascade do |t|
    t.integer "cost", default: 5
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "phistories", force: :cascade do |t|
    t.string "hname", default: "Name", null: false
    t.integer "hphone", default: 0, null: false
    t.integer "hcost", default: 0, null: false
    t.datetime "hbookdt"
    t.string "hlocat", default: "Location", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", default: 0, null: false
  end

  create_table "presentations", force: :cascade do |t|
    t.string "name", default: "not specified"
    t.text "descrip", default: "not specified"
    t.datetime "dt"
    t.string "locat", default: "not specified"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "course_id", default: 0, null: false
    t.integer "author_id", default: 0, null: false
    t.integer "attending", default: 0, null: false
  end

  create_table "umsgs", force: :cascade do |t|
    t.text "udescp", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name", default: "", null: false
    t.integer "phone", default: 0, null: false
    t.boolean "admin", default: false
    t.integer "course_id", default: 0, null: false
    t.boolean "analyse", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
