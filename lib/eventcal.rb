class Eventcal < Struct.new(:view, :date, :callback)
    EVENTH = %w[Sunday Monday Tuesday Wednesday Thursday Friday Saturday]
    SDAY = :sunday
 
    delegate :content_tag, to: :view
 
    def table
      content_tag( :table, class: "eventcal table table-bordered table-striped") do
        header + week_rows
      end
    end
 
    def header
      content_tag :tr do
        EVENTH.map { |eventd| content_tag :th, eventd }.join.html_safe
      end
    end
 
    def week_rows
      weeks.map do |eventw|
        content_tag :tr do
          eventw.map { |eventd| day_cell(eventd) }.join.html_safe
        end
      end.join.html_safe
    end
 
    def day_cell(eventd)
      content_tag :td, view.capture(eventd, &callback), class: day_classes(eventd)
    end
 
    def day_classes(eventd)
      classes = []
      classes << "today" if eventd == Date.today
      classes << "notmonth" if eventd.month != date.month
      classes.empty? ? nil : classes.join(" ")
    end
 
    def weeks
      first = date.beginning_of_month.beginning_of_week(SDAY)
      last = date.end_of_month.end_of_week(SDAY)
      (first..last).to_a.in_groups_of(7)
    end
    
end