class Notifyuser
	include Wisper::Publisher
	
	def ucall(acost,apresent)
	    @ptcost = Pcost.find_by id: (acost)

	    # business logic...

	    if @ptcost.cost < 10
	      broadcast(:sendmsg, @ptcost, apresent)
	    end
  	end
end