require 'rails_helper'

feature "sign up" do
    let(:user) {FactoryGirl.build(:user)}

    def fill_in_sign_up_fields
        fill_in "user[email]", with: user.email
        fill_in "user[name]", with: user.name
        fill_in "user[phone]", with: user.phone
        fill_in "user[password]", with: user.password
        fill_in "user[password_confirmation]", with: user.password_confirmation
        click_button "Sign up"
    end

    scenario "visit the site to sign up" do
        visit new_user_registration_path
        #click_link "Sign in"
        fill_in_sign_up_fields
        expect(page).to have_content("Welcome! You have signed up successfully.")
    end
end