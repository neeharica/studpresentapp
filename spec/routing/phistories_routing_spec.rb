require "rails_helper"

RSpec.describe PhistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/phistories").to route_to("phistories#index")
    end

    it "routes to #new" do
      expect(:get => "/phistories/new").to route_to("phistories#new")
    end

    it "routes to #show" do
      expect(:get => "/phistories/1").to route_to("phistories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/phistories/1/edit").to route_to("phistories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/phistories").to route_to("phistories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/phistories/1").to route_to("phistories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/phistories/1").to route_to("phistories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/phistories/1").to route_to("phistories#destroy", :id => "1")
    end

  end
end
