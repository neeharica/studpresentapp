FactoryGirl.define do
  sequence(:name)  { |n| "Person #{n}" }
  sequence(:email) { |n| "person_#{n}@example.com" }
  factory :user do
    email
    name
    phone     "0899660265"
    password  "sauceboss"
    password_confirmation   "sauceboss"
  end
end