require 'rails_helper'

RSpec.describe "presentations/edit", type: :view do
  before(:each) do
    @presentation = assign(:presentation, Presentation.create!(
      :name => "MyString",
      :descrip => "MyText",
      :locat => "MyString"
    ))
  end

  it "renders the edit presentation form" do
    render

    assert_select "form[action=?][method=?]", presentation_path(@presentation), "post" do

      assert_select "input[name=?]", "presentation[name]"

      assert_select "textarea[name=?]", "presentation[descrip]"

      assert_select "input[name=?]", "presentation[locat]"
    end
  end
end
