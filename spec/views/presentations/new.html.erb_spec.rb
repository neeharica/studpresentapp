require 'rails_helper'

RSpec.describe "presentations/new", type: :view do
  before(:each) do
    assign(:presentation, Presentation.new(
      :name => "MyString",
      :descrip => "MyText",
      :locat => "MyString"
    ))
  end

  it "renders new presentation form" do
    render

    assert_select "form[action=?][method=?]", presentations_path, "post" do

      assert_select "input[name=?]", "presentation[name]"

      assert_select "textarea[name=?]", "presentation[descrip]"

      assert_select "input[name=?]", "presentation[locat]"
    end
  end
end
