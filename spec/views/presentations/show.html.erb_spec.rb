require 'rails_helper'

RSpec.describe "presentations/show", type: :view do
  before(:each) do
    @presentation = assign(:presentation, Presentation.create!(
      :name => "Name",
      :descrip => "MyText",
      :locat => "Locat"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Locat/)
  end
end
