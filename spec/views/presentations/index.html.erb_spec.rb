require 'rails_helper'

RSpec.describe "presentations/index", type: :view do
  before(:each) do
    assign(:presentations, [
      Presentation.create!(
        :name => "Name",
        :descrip => "MyText",
        :locat => "Locat"
      ),
      Presentation.create!(
        :name => "Name",
        :descrip => "MyText",
        :locat => "Locat"
      )
    ])
  end

  it "renders a list of presentations" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Locat".to_s, :count => 2
  end
end
