require 'rails_helper'

RSpec.describe "phistories/new", type: :view do
  before(:each) do
    assign(:phistory, Phistory.new(
      :hname => "MyString",
      :hphone => 1,
      :hcost => 1,
      :hlocat => "MyString"
    ))
  end

  it "renders new phistory form" do
    render

    assert_select "form[action=?][method=?]", phistories_path, "post" do

      assert_select "input[name=?]", "phistory[hname]"

      assert_select "input[name=?]", "phistory[hphone]"

      assert_select "input[name=?]", "phistory[hcost]"

      assert_select "input[name=?]", "phistory[hlocat]"
    end
  end
end
