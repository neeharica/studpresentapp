require 'rails_helper'

RSpec.describe "phistories/show", type: :view do
  before(:each) do
    @phistory = assign(:phistory, Phistory.create!(
      :hname => "Hname",
      :hphone => 2,
      :hcost => 3,
      :hlocat => "Hlocat"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Hname/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Hlocat/)
  end
end
