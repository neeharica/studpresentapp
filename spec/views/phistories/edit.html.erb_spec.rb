require 'rails_helper'

RSpec.describe "phistories/edit", type: :view do
  before(:each) do
    @phistory = assign(:phistory, Phistory.create!(
      :hname => "MyString",
      :hphone => 1,
      :hcost => 1,
      :hlocat => "MyString"
    ))
  end

  it "renders the edit phistory form" do
    render

    assert_select "form[action=?][method=?]", phistory_path(@phistory), "post" do

      assert_select "input[name=?]", "phistory[hname]"

      assert_select "input[name=?]", "phistory[hphone]"

      assert_select "input[name=?]", "phistory[hcost]"

      assert_select "input[name=?]", "phistory[hlocat]"
    end
  end
end
