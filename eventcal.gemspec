Gem::Specification.new do |s|
  s.name        = 'eventcal'
  s.version     = '0.0.0'
  s.date        = '2010-04-28'
  s.summary     = "Event Calendar"
  s.description = "Event Calndar Gem"
  s.authors     = ["Neeharica"]
  s.email       = 'x16131665@student.ncirl.ie'
  s.files       = ["lib/eventcal.rb"]
  s.homepage    =
    'http://rubygems.org/gems/eventcal'
  s.license       = 'MIT'
end